using Player;
using UnityEngine;

namespace Camera
{
    public class CameraRotation : MonoBehaviour
    {
        [SerializeField] private PlayerMove _playerMove;
        [SerializeField] private float _lerpRate;
        private Rigidbody _playerRigidbody;

        private void Start() =>
            _playerRigidbody = _playerMove.GetComponent<Rigidbody>();

        private void LateUpdate() =>
            SetCameraRotation();


        private void SetCameraRotation()
        {
            Vector3 velocity = _playerRigidbody.velocity;
            velocity.y = 0;
            
            if (Mathf.Abs(velocity.magnitude) < 0.02f)
                return;
            var newRotation = Quaternion.LookRotation(velocity);
            transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, _lerpRate * Time.deltaTime);
        }
    }
}