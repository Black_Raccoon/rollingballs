using System;
using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

public class DeadZone : MonoBehaviour
{
    [SerializeField] private LevelMenu levelMenu;


    private void OnValidate()
    {
        if (levelMenu == null)
            levelMenu = FindObjectOfType<LevelMenu>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent(out PlayerMove _))
            levelMenu.ReplayGame();
    }
}