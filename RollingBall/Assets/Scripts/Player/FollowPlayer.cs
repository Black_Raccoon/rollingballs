using UnityEngine;

namespace Player
{
    [ExecuteAlways]
    public class FollowPlayer : MonoBehaviour
    {
        [SerializeField] private PlayerMove _playerMove;

        private void Update() => 
            transform.position = _playerMove.transform.position;
    }
}
