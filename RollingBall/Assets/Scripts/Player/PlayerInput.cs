﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerInput : MonoBehaviour
    {
        public event Action Jump;
        public event Action Pause;

        public Vector2 MoveAxis => _input.Player.Move.ReadValue<Vector2>();

        private Controls _input;

        public static PlayerInput Instance;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
                Destroy(Instance.gameObject);

            _input = new();
            _input.Player.Jump.performed += HandleJumpInput;
            _input.Game.Pause.performed += HandlePauseInput;
        }

        private void OnEnable()
        {
            _input.Enable();
        }


        private void OnDisable()
        {
            _input.Disable();
            _input.Player.Jump.performed -= HandleJumpInput;
            _input.Game.Pause.performed -= HandlePauseInput;
        }

        public void EnableMoveInput()
        {
            _input.Player.Enable();
        }

        public void DisableMoveInput()
        {
            _input.Player.Disable();
            InputBinding newPath = new InputBinding("<Keyboard>/enter");
            _input.Player.Jump.ApplyBindingOverride(newPath);
        }

        private void HandleJumpInput(InputAction.CallbackContext obj) =>
            Jump?.Invoke();

        private void HandlePauseInput(InputAction.CallbackContext obj)
        {
            Pause?.Invoke();
        }
    }
}