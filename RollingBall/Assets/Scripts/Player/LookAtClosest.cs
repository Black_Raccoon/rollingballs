using Coins;
using UnityEngine;

namespace Player
{
    public class LookAtClosest : MonoBehaviour
    {
        [SerializeField] private CoinSpawner _coinSpawner;
        [SerializeField] private float _getClosestDeltaTime;
        [SerializeField] private float _getClosestInRadius;
        [SerializeField] private bool _drawGizmo;
        [SerializeField] private Renderer _arrowRenderer;
        private Transform _closest;
        private float _timer;


        private void Update()
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                _arrowRenderer.enabled = true;
                GetClosestWithTimer();
                RotateToClosest();
            }
            else
                _arrowRenderer.enabled = false;
        }

        private void RotateToClosest()
        {
            if (_closest != null)
                transform.LookAt(new Vector3(_closest.position.x, 0, _closest.position.z));
        }

        private void GetClosestWithTimer()
        {
            _timer -= Time.deltaTime;
            if (_timer < 0)
            {
                GetClosest();
                _timer = _getClosestDeltaTime;
            }
        }

        private void GetClosest()
        {
            Collider[] overlapColliders = Physics.OverlapSphere(transform.position, _getClosestInRadius);
            float minSqrDistance = Mathf.Infinity;
            _closest = null;

            var i = 0;
            foreach (Collider overlapCollider in overlapColliders)
            {
                if (overlapCollider.gameObject.TryGetComponent(out Coin coin))
                {
                    i++;
                    float sqrDistance = (Vector3.SqrMagnitude(coin.transform.position - transform.position));
                    if (sqrDistance < minSqrDistance)
                    {
                        minSqrDistance = sqrDistance;
                        _closest = coin.transform;
                    }
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (_drawGizmo)
            {
                Gizmos.color = new Color(0, 0, 1, 0.1f);
                Gizmos.DrawSphere(transform.position, _getClosestInRadius);
            }
        }
    }
}