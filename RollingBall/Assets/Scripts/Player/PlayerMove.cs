using NaughtyAttributes;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerMove : MonoBehaviour
    {
        private float VerticalInput => _playerInput.MoveAxis.y;
        private float HorizontalInput => _playerInput.MoveAxis.x;
        private bool IsOnGround => Physics.Raycast(transform.position, Vector3.down, _groundCheckOffset, whatIsGround);

        [SerializeField] private float _torqueValue;
        [SerializeField] private float _maxAngularVelocity;
        [SerializeField] private Transform _cameraCenter;
        [SerializeField] private float _jumpForce;
        [SerializeField] private int _auxAirJumpCount;
        [SerializeField] private float _flyingSpeed;
        [SerializeField] private float _groundCheckOffset;
        [SerializeField] private LayerMask whatIsGround;
        [ShowNonSerializedField] private bool _isFlying;
        [SerializeField] private Rigidbody _rb;
        [SerializeField] private PlayerInput _playerInput;
        private bool _wouldLikeJump;
        private int _airJumpCount;


        private void Start() =>
            _rb.maxAngularVelocity = _maxAngularVelocity;

        private void OnEnable() =>
            _playerInput.Jump += OnJumpInput;

        private void OnDisable() =>
            _playerInput.Jump -= OnJumpInput;

        private void OnJumpInput() =>
            _wouldLikeJump = true;

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position + Vector3.down * _groundCheckOffset, 0.1f);
        }


        private void FixedUpdate()
        {
            Moving();

            if (_wouldLikeJump)
                HandleJumpInput();
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (IsOnGround)
            {
                _isFlying = false;
                _airJumpCount = 0;
            }
        }

        private void HandleJumpInput()
        {
            _wouldLikeJump = false;
            if (!IsOnGround)
                if (_airJumpCount < _auxAirJumpCount)
                    _airJumpCount++;
                else
                    return;
            PerformJump();
        }

        private void PerformJump()
        {
            _isFlying = true;
            _rb.AddForce(_cameraCenter.up * _jumpForce);
        }

        private void Moving()
        {
            RollingProcess();

            if (_isFlying)
                FlyingProcess();
        }

        private void FlyingProcess()
        {
            //new Vector2()
            _rb.AddForce(_cameraCenter.forward * VerticalInput * _flyingSpeed);
            _rb.AddForce(_cameraCenter.right * HorizontalInput * _flyingSpeed);
        }

        private void RollingProcess()
        {
            Debug.Log($"VerticalInput = {VerticalInput}, HorizontalInput ={HorizontalInput} ");
            _rb.AddTorque(_cameraCenter.right * VerticalInput * _torqueValue);
            _rb.AddTorque(_cameraCenter.forward * -HorizontalInput * _torqueValue);
        }

        private void OnValidate()
        {
            if (_playerInput == null)
                _playerInput = FindObjectOfType<PlayerInput>();
            if (_rb == null)
                _rb = GetComponent<Rigidbody>();
        }
    }
}