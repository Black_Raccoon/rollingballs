using UnityEngine;

namespace Player
{
    public class RotateWithVelocity : MonoBehaviour
    {
        [SerializeField] private PlayerMove _playerMove;
        [SerializeField] private float _lerpRate;
        private Rigidbody _playerRigidbody;

        private void Start() =>
            _playerRigidbody = _playerMove.GetComponent<Rigidbody>();

        private void Update() =>
            Rotate();


        private void Rotate()
        {
            var velocity = _playerRigidbody.velocity;
            velocity.y = 0;

            var newRotation = Quaternion.LookRotation(velocity);
            transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, _lerpRate * Time.deltaTime);
        }
    }
}