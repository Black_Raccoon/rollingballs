using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Coins
{
    public class CoinSpawner : MonoBehaviour
    {
        public int CoinsCount => _coins.Count;
        public event Action<int> OnCoinsCountChanged;
        public event Action OnAllCoinsCollected;

        [SerializeField] private int _count;
        [SerializeField] private Coin _coinPrefab;
        [SerializeField] private Transform _cornerA;
        [SerializeField] private Transform _cornerB;
        [SerializeField] private AudioSource _collectSound;
        [SerializeField] private bool _drawGizmo;
        [SerializeField] private UnityEvent _coinCollected;
        private List<Coin> _coins = new();


        private void Start() =>
            SpawnCoins();

        public void Collect(Coin coin)
        {
            _coins.Remove(coin);
            coin.Collect();
            _coinCollected.Invoke();
            OnCoinsCountChanged?.Invoke(_coins.Count);
            if (_coins.Count <= 0)
                OnAllCoinsCollected?.Invoke();
        }

        private void SpawnCoins()
        {
            for (int i = 0; i < _count; i++)
            {
                GetRotation();
                var coin = Instantiate(_coinPrefab, GetPosition(), GetRotation(), transform);
                coin.Construct(this);
                _coins.Add(coin);
            }

            OnCoinsCountChanged?.Invoke(_coins.Count);
        }

        private Vector3 GetPosition()
        {
            Vector3 position;
            position.x = Random.Range(_cornerA.position.x, _cornerB.position.x);
            position.y = Random.Range(_cornerA.position.y, _cornerB.position.y);
            position.z = Random.Range(_cornerA.position.z, _cornerB.position.z);
            return position;
        }

        private Quaternion GetRotation()
        {
            var angle = Random.Range(0f, 360f);
            return Quaternion.AngleAxis(angle, Vector3.up);
        }

        private void OnDrawGizmos() =>
            DrawSpawnZone();

        private void DrawSpawnZone()
        {
            if (_drawGizmo)
            {
                Gizmos.color = new Color(0f, 1f, 0f, 0.2f);
                Vector3 center = (_cornerA.position + _cornerB.position) / 2;
                Vector3 size = _cornerA.position - _cornerB.position;
                Gizmos.DrawCube(center, size);
            }
        }
    }
}