using DG.Tweening;
using NaughtyAttributes;
using Player;
using UnityEngine;

namespace Coins
{
    public class Coin : MonoBehaviour
    {
        [SerializeField] private float _rotationSpeed;
        private CoinSpawner _coinSpawner;
        [Foldout("Show")] [SerializeField] private Ease showEase;
        [Foldout("Show")] [SerializeField] private float showDuration;
        [Foldout("Show")] [SerializeField] private float showPeriod;
        [Foldout("Show")] [SerializeField] private float showAmplitude;

        [Foldout("Move")] [SerializeField] private Ease MoveshowEase;
        [Foldout("Move")] [SerializeField] private float MoveshowDuration;
        [Foldout("Move")] [SerializeField] private float MoveshowPeriod;
        [Foldout("Move")] [SerializeField] private float MoveshowAmplitude;

        public void Construct(CoinSpawner coinSpawner) =>
            _coinSpawner = coinSpawner;

        private void Update() =>
            transform.rotation *= Quaternion.AngleAxis(_rotationSpeed * Time.deltaTime, Vector3.up);

        private void OnTriggerEnter(Collider other)
        {
            if (other.attachedRigidbody == null)
                return;

            if (other.attachedRigidbody.TryGetComponent(out PlayerMove _))
                _coinSpawner.Collect(this);
        }

        [Button()]
        public void Collect()
        {
            transform.DOLocalMoveY(2, MoveshowDuration).SetEase(MoveshowEase, MoveshowAmplitude, MoveshowPeriod);
            transform.DOScale(Vector3.zero, showDuration).SetEase(showEase, showAmplitude, showPeriod)
                .OnComplete(() => Destroy(gameObject));
        }
    }
}