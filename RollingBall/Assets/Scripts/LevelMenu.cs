using System;
using DG.Tweening;
using NaughtyAttributes;
using Player;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelMenu : MonoBehaviour
{
    [SerializeField] private GameObject _menu;
    [SerializeField] private float offsetDistance;

    [Foldout("Show")] [SerializeField] private Ease showEase;
    [Foldout("Show")] [SerializeField] private float showDuration;
    [Foldout("Show")] [SerializeField] private float showPeriod;
    [Foldout("Show")] [SerializeField] private float showAmplitude;

    [Foldout("Hide")] [SerializeField] private Ease hideEase;
    [Foldout("Hide")] [SerializeField] private float hideDuration;
    [Foldout("Hide")] [SerializeField] private float hidePeriod;
    [Foldout("Hide")] [SerializeField] private float hideAmplitude;

    private float _showPosition;
    private float _hidePosition;
    private bool _isPaused;
    private PlayerInput _input;

    private void OnValidate()
    {
        if (_input == null)
            _input = FindObjectOfType<PlayerInput>();
    }

    private void Awake()
    {
        _showPosition = _menu.transform.localPosition.y;
        _hidePosition = _showPosition + offsetDistance;
        _menu.transform.DOLocalMoveY(_hidePosition, 0);
    }

    private void OnEnable()
    {
        _input.Pause += PressPauseHandle;
    }

    private void OnDisable() =>
        _input.Pause -= PressPauseHandle;

    private void PressPauseHandle()
    {
        if (!_isPaused)
            OnPause();
        else
            OnResume();
    }

    [Button()]
    public void OnPause()
    {
        _isPaused = true;
        _menu.transform.DOLocalMoveY(_showPosition, showDuration).SetEase(showEase, showAmplitude, showPeriod);
        PlayerInput.Instance.DisableMoveInput();
    }

    [Button]
    public void OnResume()
    {
        _isPaused = false;
        _menu.transform.DOLocalMoveY(_hidePosition, hideDuration).SetEase(hideEase, hideAmplitude, hidePeriod);
        PlayerInput.Instance.EnableMoveInput();
    }

    public void ReplayGame() =>
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);

    public void StartNextLevel() =>
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);

    public void ExitGame() =>
        Application.Quit();
}