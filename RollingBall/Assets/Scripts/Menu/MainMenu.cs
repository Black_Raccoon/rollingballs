using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class MainMenu : MonoBehaviour
    {
        [Scene] [SerializeField] private string firstSceneName;

        public void PlayGame()
        {
            SceneManager.LoadSceneAsync(firstSceneName);
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}