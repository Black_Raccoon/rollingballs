using System;
using Coins;
using UnityEngine;
using UnityEngine.UI;

public class LeftCoinsUI : MonoBehaviour
{
    [SerializeField] private CoinSpawner _coinSpawner;
    [SerializeField] private Text _leftCoinsText;


    private void OnEnable()
    {
        _coinSpawner.OnCoinsCountChanged += UpdateUIText;
        UpdateUIText(_coinSpawner.CoinsCount);
    }

    private void UpdateUIText(int coinsCount)
    {
        _leftCoinsText.text = coinsCount.ToString();
    }

    private void OnDisable()
    {
        _coinSpawner.OnCoinsCountChanged -= UpdateUIText;
    }

    private void OnValidate()
    {
        if (_coinSpawner == null)
            _coinSpawner = FindObjectOfType<CoinSpawner>();
    }
}