using Coins;
using DG.Tweening;
using UnityEngine;

public class WinPanel : MonoBehaviour
{
    [SerializeField] private Ease showEase;
    [SerializeField] private float showDuration;
    [SerializeField] private float showAmplitude;
    [SerializeField] private float showPeriod;
    [SerializeField] private float offsetDistance;

    [SerializeField] private CoinSpawner _coinSpawner;

    private float _showPosition;
    private float _hidePosition;

    private void Awake()
    {
        _showPosition = transform.localPosition.y;
        _hidePosition = _showPosition + offsetDistance;
        transform.DOLocalMoveY(_hidePosition, 0);
    }

    private void OnEnable()
    {
        _coinSpawner.OnAllCoinsCollected += Show;
    }

    private void Show()
    {
        transform.DOLocalMoveY(_showPosition, showDuration).SetEase(showEase, showAmplitude, showPeriod);
    }

    private void OnValidate()
    {
        if (_coinSpawner == null)
            _coinSpawner = FindObjectOfType<CoinSpawner>();
    }
}